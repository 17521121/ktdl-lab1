import numpy as np
import pandas as pd
import math

progName = input("Ten chuong trinh: ")
option = input("Tuy chon: ")
inputPath = input("Tap tin dau vao: ")
outputPath = input("Tap tin dau ra: ")
log = input("Tap tin log: ")

#delete after finish
# progName = "lab1"
# option = "normalize" # 4 option
# inputPath = "Abalone/testTH1.csv"
# outputPath = "output.txt"
# log = "log.txt"
#end

#Binning by equal width
def equalWidth(column, m): 
    try:
        w = float((max(column) - min(column))) / float(m)
    except ZeroDivisionError as detail:
        print("Loi chia cho khong",detail)
    minOfCol = min(column) 
    result = [] 
    for i in range(0, m + 1): 
        result = result + [minOfCol + w * i] 
    return result

# Bining equal depth
def equalDepth(column, m): 
    size = len(column) 
    n = int(size / m) 
    arrResult = []
    for i in range(0, m): 
        temp = [] 
        for j in range(i * n, (i + 1) * n): 
            if j >= size: 
                break
            temp = temp + [column[j]]
        arrResult.append(temp)
    return arrResult

# Try convert a full column to float, is numeric if succeed else nominal
def isNumeric(column):
    #columnWorking = filterMissingValue(column)
    try:
        column.astype(float)
        return True
    except ValueError:
        return False

#Skip missing value by column
def filterMissingValue(column):
    arrResult = []
    for val in column:
        if (val == "?"):
            continue
        else:
            arrResult.append(val)
    return np.array(arrResult)

#Average value of a column
def meanColumn(column):
    x = 0.0
    for i in column:
        x = x + float(i)
    x /= (column.shape[0])
    return round(x, 3)

#Value in column appeared the most
def appearedMost(column):
    name = []
    appearedTime = []
    
    for val in column:
        if ( len(name) == 0 or val not in name):
            name.append(val)
            appearedTime.append(1)
        else:
            appearedTime[name.index(val)]+=1
    
    return name[appearedTime.index(max(appearedTime))]

def min_max(v, arrFeature, new_min = 0.0, new_max = 1.0):
    return float((float((v - min(arrFeature)))/(float(max(arrFeature)-min(arrFeature)))*float((new_max - new_min))) + new_min)

def z_score(v, arrFeature):
    meanValueOfFeature = float(sum(arrFeature))/float(len(arrFeature))
    sumstd = 0
    temp = 0
    for val in arrFeature:
        temp = (val - meanValueOfFeature)**2
        sumstd +=temp

    standardFeature = math.sqrt(float(1.0/float(len(arrFeature)))*float(sumstd))

    return float(float(v)-meanValueOfFeature)/float(standardFeature)

def checkFloat(val):
    try:
        float(val)
        return True
    except ValueError:
        return False
    
if(option.lower() == "summary"):  # Tom tat du lieu
    data = pd.read_csv(inputPath)
    logFile = open(log, "w+")
    logFile.write("#So mau: %s\n" % len(data))
    logFile.write("#So thuoc tinh: %s\n" % data.shape[1])
    for i, val in enumerate(data.columns):
        is_Numeric = isNumeric(filterMissingValue(data.values[:, i]))
        logFile.write("#Thuoc tinh %d: %s %s\n" % (i+1, val, "Numeric" if is_Numeric else "Nominal"))
    logFile.close()

elif(option.lower() == "replace"):
    data = pd.read_csv(inputPath)
    fillMissingValue = []
    logFile = open(log, "w+")
    for i, val in enumerate(data.columns):
        workingData = filterMissingValue(data.values[:, i])
        if(isNumeric(workingData)):
            fillMissingValue.append(meanColumn(workingData))
        else:
            fillMissingValue.append(appearedMost(workingData))
        logFile.write("#Thuoc tinh: %s, %d, %s\n" % (val, data.values[:, i].shape[0] - workingData.shape[0], fillMissingValue[i]))    
    logFile.close()

    #Export output
    with open(outputPath, "w+") as outputFile:
        outputFile.write(",".join(data.columns))
        outputFile.write("\n")
        for row in data.values:
            for index, valRow in enumerate(row):
                if(valRow == "?"):
                    row[index]=fillMissingValue[index]
                outputFile.write("%s," % row[index])
            outputFile.seek(outputFile.tell()-1)
            outputFile.write("\n")
        outputFile.close()

elif(option.lower() == "discretize"):
    numbOfBinning = 40
    quantityOfBinning = np.zeros(numbOfBinning).tolist()

    data = pd.read_csv(inputPath)
    logFile = open(log,"w+")
    nameFeature = data.columns
    arrbinningofFeature = []
    for feature in nameFeature:
        workingData = filterMissingValue(data[feature].values)
        if(isNumeric(workingData)):
            workingData = workingData.astype(float)
            arrbinning = equalWidth(workingData, numbOfBinning) 
            arrbinningofFeature.append(arrbinning)
            for iBin in range(len(arrbinning)-1):
                for val in workingData:
                    if(arrbinning[iBin] <= val and val < arrbinning[iBin + 1]):
                        quantityOfBinning[iBin] += 1
            logFile.write("Thuoc tinh: %s " % feature)
            for index, valBin in enumerate(quantityOfBinning):
                logFile.write("[%f; %f]: %d\t" % ( arrbinning[index], arrbinning[index +1], valBin))
            logFile.write("\n")
        
    logFile.close()
    outputFile = open(outputPath, "w+")
    outputFile.write(''.join('%s,' % x for x in data.columns))
    outputFile.write('\n')
                                    
    with open(outputPath, "w+") as outputFile:
        outputFile.write(''.join('%s,' % x for x in data.columns))
        outputFile.write('\n')
        for row in data.values:
            for index, valRow in enumerate(row):
                if (row[index]=="?"):
                    outputFile.write('%s,'% row[index])
                elif (checkFloat(row[index])):
                    for arrbinning in arrbinningofFeature:
                        for iBin in range(len(arrbinning)-1):
                            if(arrbinning[iBin].astype(float) <= float(row[index]) and float(row[index]) <= arrbinning[iBin + 1].astype(float)):
                                outputFile.write('[%f:%f],' %(arrbinning[iBin],arrbinning[iBin+1]))
                elif (checkFloat(row[index]) == False):
                    outputFile.write('%s,'% row[index])
            outputFile.seek(outputFile.tell()-1)
            outputFile.write("\n")
        outputFile.close()

elif(option.lower() == "normalize"):
    
    data = pd.read_csv(inputPath)
    nameFeature = data.columns
    normal = input("Kieu chuan hoa( min-max hay z-score): ")
    logFile = open(log,"w+")
    arrWorkingData = []
    arrvalueNormal = []
    for feature in nameFeature:
        workingData = filterMissingValue(data[feature].values)
        arrWorkingData.append(workingData)
        if(isNumeric(workingData)):
            workingData = workingData.astype(float)
            arrWorkingData[-1] = workingData
            valueNormal = []
            for index, value in enumerate(workingData):
                if (normal.lower()=="min-max"):
                    valueNormal.append(min_max(value ,workingData))
                elif(normal.lower()=="z-score"):
                    valueNormal.append(z_score(value, workingData))
                    
            logFile.write("Thuoc tinh: %s , [%f , %f] " % (feature, min(valueNormal), max(valueNormal)))
            logFile.write("\n")
            arrvalueNormal.append(valueNormal)
    logFile.close()
    with open(outputPath, "w+") as outputFile:
        outputFile.write(''.join('%s,' % x for x in data.columns))
        outputFile.write('\n')
        for row in data.values:
            for index, valRow in enumerate(row):
                if (row[index]=="?"):
                    outputFile.write('%s,'% row[index])
                elif (isNumeric(filterMissingValue(data.values[:,index]))):
                    if (normal.lower()=="min-max"):
                        v_new = min_max(float(row[index]),arrWorkingData[index],0.0,1.0)
                        outputFile.write('%f,'% v_new)
                    elif(normal.lower()=="z-score"):
                        v_new = z_score(float(row[index]),arrWorkingData[index])
                        outputFile.write('%f,'% v_new)
                else:
                    outputFile.write('%s,'% row[index])
            outputFile.seek(outputFile.tell()-1)
            outputFile.write("\n")
        outputFile.close()
    
print("<%s> <%s> <input: %s> <output: %s> <log: %s>" %(progName,option,inputPath,outputPath,log))